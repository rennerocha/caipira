# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic.simple import direct_to_template

from palestras.models import PropostaPalestra

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'caipira.views.home', name='home'),
    # url(r'^caipira/', include('caipira.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', direct_to_template, {'template': 'index.html', 'extra_context': {'page': 'index'} }, name='index'),
    url(r'^oevento/$', direct_to_template, {'template': 'oevento.html', 'extra_context': {'page': 'oevento' } }, name='oevento'),
    url(r'^edicao2011/$', direct_to_template, {'template': 'edicao2011.html', 'extra_context': {'page': 'edicao2011' } }, name='edicao2011'),
    url(r'^localizacao/$', direct_to_template, {'template': 'localizacao.html', 'extra_context': {'page': 'localizacao'} }, name='localizacao'),
    url(r'^programacao/$', direct_to_template, {'template': 'programacao.html', 'extra_context': {'page': 'programacao'} }, name='programacao'),
    url(r'^programacao_detalhada/$', direct_to_template, {'template': 'programacao_detalhada.html', 'extra_context': {'page': 'programacao'} }, name='programacao_detalhada'),
    url(r'^organizadores/$', direct_to_template, {'template': 'organizadores.html', 'extra_context': {'page': 'organizadores'} }, name='organizadores'),
    url(r'^inscricoes/$', direct_to_template, {'template': 'inscricoes.html', 'extra_context': {'page': 'inscricoes'} }, name='inscricoes'),

    url(r'^submissao/$', 'palestras.views.submissao', name='submissao'),
    url(r'^submetidas/$', 'django.views.generic.list_detail.object_list',
        {'queryset': PropostaPalestra.objects.filter(visivel=True).order_by('titulo'), 'template_name': 'propostapalestra_list.html', }, name='submetidas'),

    url(r'^talks/$', direct_to_template, {'template': 'palestras.html', 'extra_context': {'page': 'programacao'} }, name='programacao'),
)

urlpatterns += staticfiles_urlpatterns()
