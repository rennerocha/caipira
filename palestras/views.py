#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext

from palestras.forms import PropostaPalestraForm


def submissao(request):
    page = 'submissao'
    palestra_submetida = False
    form = PropostaPalestraForm(request.POST or None)

    if form.is_valid():
        form.save()
        palestra_submetida = True

    return render_to_response('submissao.html', 
                              {'form': form, 'palestra_submetida': palestra_submetida, },
                              RequestContext(request))
