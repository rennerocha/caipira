#-*- coding: utf-8 -*-
from django.db import models


class PropostaPalestra(models.Model):
    titulo = models.CharField(max_length=100, verbose_name=u'Título')
    descricao = models.TextField(verbose_name=u'Descrição')

    palestrante = models.CharField(max_length=100, verbose_name=u'Palestrante')
    email_palestrante = models.EmailField(verbose_name=u'E-mail de Contato')
    mini_bio_palestrante = models.TextField(verbose_name=u'Mini-biografia do Palestrante')

    visivel = models.BooleanField(default=False)
    data_submissao = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return self.titulo
