#-*- coding: utf-8 -*-
from django.forms import ModelForm
from palestras.models import PropostaPalestra


class PropostaPalestraForm(ModelForm):

    class Meta:
        model = PropostaPalestra
        exclude = ('visivel', 'data_submissao', )

